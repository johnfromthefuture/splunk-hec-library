#!/usr/bin/env python3
'''
Copyright (C) 2020 John Landers

This program is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
'''


import sys
import queue as Queue
import json
from datetime import datetime
import time

from splunk_hec import splunk_hec


def generate_test_data( hec_token, hec_server, num_test_events=5000, hec_port='8088', input_type='raw', use_hec_tls=True, use_ack=False, hec_tls_verify=False, ack_interval=15, max_ack_attempts=15, max_content_length=50000000, 
    max_events_per_batch=1000, use_backup_queue=True, context_id='generate-test-data', rotate_session_after=5, debug_enabled=True, max_threads=0, disable_tls_validation_warnings=True ):

    start_time = float(datetime.now().strftime('%s'))

    splhec = splunk_hec( token=hec_token, hec_server=hec_server, hec_port=hec_port, input_type=input_type, use_hec_tls=use_hec_tls, use_ack=use_ack, hec_tls_verify=hec_tls_verify, ack_interval=ack_interval,
        max_ack_attempts=max_ack_attempts, max_content_length=max_content_length, max_events_per_batch=max_events_per_batch, context_id=context_id, rotate_session_after=rotate_session_after, debug_enabled=debug_enabled,
        max_threads=max_threads, disable_tls_validation_warnings=disable_tls_validation_warnings )

    if use_backup_queue:
        splhec.backup_queue = Queue.Queue(0)

    index = 'main'
    sourcetype = 'hec:%s' % str(context_id)
    source = 'hec:test:events'

    if 'raw' in input_type:
        splhec.set_request_params({'index':index, 'sourcetype':sourcetype, 'source':source})

    for i in range(num_test_events):
        if 'json' in input_type:
            payload = {}
            payload['event'] = '{"message": "JSON test Message %s.", "foo": "bar"}' % str(i)
            payload['time'] = int(datetime.utcnow().strftime('%s'))
            payload['source'] = source
            payload['index'] = index
            payload['sourcetype'] = sourcetype
            payload = json.dumps(payload)    
        else:
            payload = '%s RAW Test Message %s.' % (str(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S -0000')),str(i))

        # This should add the event to our queue
        splhec.send_event(payload)


    splhec.stop_threads_and_processing()


    end_time = float(datetime.now().strftime('%s'))
    total_exec_time = end_time-start_time

    print('Testing took %s seconds for %s with %s num_threads and %s events per batch.' % (str(total_exec_time), str(num_test_events), str(max_threads), str(max_events_per_batch)))



# Target splunk server
hec_server='sh01'

# Token with ACK enabled
hec_token_ack='d36610ec-8e72-4514-9400-96ebe23ae923'

# Token with ACK disabled
hec_token_noack='73350b4a-7f7b-460c-8099-fd9bdd2b68d4'

## Test 1: ACK disabled, Threading disabled, raw input.
generate_test_data( hec_token=hec_token_noack, hec_server=hec_server, context_id='raw-noack-nothreading' )
time.sleep(5)

## Test 2: ACK disabled, Threading disabled, json input.
generate_test_data( hec_token=hec_token_noack, hec_server=hec_server, context_id='json-noack-nothreading', input_type='json' )
time.sleep(5)

## Test 3: ACK enabled, Threading disabled, raw input.
generate_test_data( hec_token=hec_token_ack, hec_server=hec_server, context_id='raw-ack-nothreading', use_ack=True )
time.sleep(5)

## Test 4: ACK enabled, Threading disabled, json input.
generate_test_data( hec_token=hec_token_ack, hec_server=hec_server, context_id='json-ack-nothreading', input_type='json', use_ack=True )
time.sleep(5)


## Test 5: ACK disabled, Threading enabled, raw input.
generate_test_data( hec_token=hec_token_noack, hec_server=hec_server, context_id='raw-noack-threading', max_events_per_batch=2500, max_threads=3, num_test_events=50000 )
time.sleep(5)

## Test 6: ACK disabled, Threading enabled, json input.
generate_test_data( hec_token=hec_token_noack, hec_server=hec_server, context_id='json-noack-threading', input_type='json', max_events_per_batch=2500, max_threads=3, num_test_events=50000 )
time.sleep(5)

## Test 7: ACK enabled, Threading enabled, raw input.
generate_test_data( hec_token=hec_token_ack, hec_server=hec_server, context_id='raw-ack-threading', use_ack=True, max_events_per_batch=2500, max_threads=3, num_test_events=50000 )
time.sleep(5)

## Test 8: ACK enabled, Threading enabled, json input.
generate_test_data( hec_token=hec_token_ack, hec_server=hec_server, context_id='json-ack-threading', input_type='json', use_ack=True, max_events_per_batch=2500, max_threads=3, num_test_events=50000 )


