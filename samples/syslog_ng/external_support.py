#!/usr/bin/env python3
'''
Copyright (C) 2020 John Landers

This program is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
'''

##### LOGGING OPTIONS #####
# Where do you want output from this script to go?
LOG_DIRECTORY = None

# What should the name of that file be?
LOG_NAME = 'syslogng_output_hec_messages.log'

# Do you want debug logs? You really don't unless you're having a problem. True/False
ENABLE_DEBUG = False

# For log rotation, what is the max file size in bytes?
MAX_LOG_SIZE_BYTES = 256000

# How many rotated files should we keep?
LOG_BACKUP_COUNT = 5

##### FAILOVER OPTIONS #####
FAILOVER_MAX_THREADS = 2

###### PROCESSING ######
# Polling period for getting data from stdin
POLLING_PERIOD = 0.2

# Number of threads to use for sending data to HEC
HEC_MAX_THREADS = 5

# During idle time, we check to see the last time we forced an event flush and we push data out to limit the amount of time data sits
# in a memory buffer...
MAX_ESTIMATED_SECONDS_BETWEEN_SENDS = 120

## HEC Customization Options
# Dictates how often to establish a new session; after X posts to the HEC, we will establish a new session
# If you are posting to indexers behind a load balancer, setting this low is ideal for data distribution
HEC_ROTATE_SESSIONS_AFTER = 10

# Maximum number of events to contain in a batch
HEC_MAX_BATCH_SIZE = 50000

# Maximum estimated size of events to contain in a single batch sent to HEC
HEC_MAX_CONTENT_LENGTH = 75000000
