#!/usr/bin/env python3
'''
Copyright (C) 2020 John Landers

This program is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
'''


import sys, os, logging, select, argparse, time, threading, re
import traceback
import queue as Queue

from logging.handlers import RotatingFileHandler
from datetime import datetime

'''
It is assumed that the library path will be in the same location as this script in the lib directory.
'''
try:
    # Add our app path to the sys path for library usage
    DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'lib')
    if not DIR in sys.path:
        sys.path.append(DIR)
    
    from external_support import LOG_DIRECTORY, LOG_NAME, ENABLE_DEBUG, MAX_LOG_SIZE_BYTES, LOG_BACKUP_COUNT, POLLING_PERIOD, FAILOVER_MAX_THREADS, HEC_MAX_THREADS, MAX_ESTIMATED_SECONDS_BETWEEN_SENDS, HEC_ROTATE_SESSIONS_AFTER, HEC_MAX_BATCH_SIZE, HEC_MAX_CONTENT_LENGTH
    
    # Import the HEC library
    from splunk_hec import splunk_hec
    
except Exception as ex:
    print('Exception occurred when attempting to import external_support and splunk_hec. Verify files exist at path %s. Exception %s' 
        % (str(DIR), str(ex)))


def failover_queue_monitor(fail_q, dirpath, logger):
    logger.info('Starting failover thread.')
    idle_count = 0
    
    while True:
        global proc_event
        '''
        This needs to do the following:
            1. Monitor the failure queue
            2. Write anything that hits the failure queue to a file in dirpath
                2a. File should be YYYYMMDDHH.log

        '''
        proc_event.wait(1.0)
        try:
            logger.debug('Attempting to get data from failover queue.')
            payload = fail_q.get(False)
            proc_event.set()
        except:
            logger.debug('No data exists. Exit thread.')
            proc_event.clear()
            idle_count += 1
            
            if idle_count >= 30:
                logger.info('Exiting thread after 30 idle cycles.')
                # Data no longer exists on the failover queue, and we've been idle for a while
                sys.exit(0)
        else:
            idle_count = 0
            #self.log(('Measurement: Queue flush requested. context=%s, num_events_in_request=%s, estimated_payload_length=%s' % (self.context_id, str(len(self.current_batch_events)), str(self.current_payload_length))))                        
            filename = str(datetime.utcnow().strftime('%Y%m%d%H')) + '.log'
            logger.debug('Found data on failover queue, writing. estimated_payload_length=%d, filename=%s', len(payload), filename )

            full_path = os.path.join(dirpath, filename)

            with open(full_path, 'a+') as f:
                f.write(payload)

            fail_q.task_done()

def start_failover_thread(thread_num, fail_q, dirpath, logger):
    th = threading.Thread(
        target=failover_queue_monitor,
        name='failover_monitor-' + str(thread_num),
        args=(fail_q, dirpath, logger))

    th.start()
    return th

def do_failure_thread_check(thread_list):
    new_list = []
    while thread_list:
        th = thread_list.pop(0)
        try:
            if th.is_alive():
                new_list.append(th)
                
        except:
            pass
            
    return new_list
    

'''
Arguments supported by the script
'''
parser = argparse.ArgumentParser()
parser.add_argument("token", help='HEC Token for Use with this Application')
parser.add_argument("server", help='HEC Event Collector Server or Load Balancer')
parser.add_argument('--port', help='HEC Port', default='8088')
parser.add_argument('--useack', help='True/False: Use Acknowledgment', default='False')
parser.add_argument('--tlsverify', help='True/False: Verify TLS certificate', default='False')
parser.add_argument('--failover', help='True/False: Send log failures to a flat file', default='False')
parser.add_argument('--dirpath', help='If failover is true, this is the directory path for the failover file', default=None)
parser.add_argument('--source', default='hec:syslog')
parser.add_argument('--sourcetype', default='syslog')
parser.add_argument('--index', default='main')
parser.add_argument('--host', default='syslog-host')

args = parser.parse_args()

'''
Now create a logger
'''
logger = logging.getLogger(__name__)

if ENABLE_DEBUG:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

# It's possible to have multiple feeds to this, so output logfiles are separated by given sourcetype.
## Problem: Multiple feeds could have same sourcetype...
try:
    log_file = str(re.sub(r'[^a-zA-Z]', "", args.sourcetype)) + '_' + LOG_NAME
except:
    log_file = LOG_NAME
    
if LOG_DIRECTORY is None:
    # Instead of exiting out, I will use a default path
    LOG_DIRECTORY = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'logs')

handler = RotatingFileHandler( str(os.path.join(LOG_DIRECTORY, log_file)), maxBytes=MAX_LOG_SIZE_BYTES, backupCount=LOG_BACKUP_COUNT)
handler.setFormatter( logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(threadName)s - %(message)s') )
logger.addHandler(handler)

logger.info('Created log handler.')

'''
Check for failover settings
'''
if str(args.failover).lower() == 'true' and (args.dirpath is None):
    parser.error("When --failover is true, --dirpath must be specified.")
    sys.exit(1)

# Create the target directory if it doesn't already exist
if args.dirpath is not None:
    if os.path.exists(args.dirpath):
        logger.debug('Provided directory path exists. Moving on.')
    else:
        logger.debug('Provided directory path does not exist. Attempting to create.')
        try:
            os.makedirs(args.dirpath)
        except:
            logger.error('Provided directory path does not exist and unable to create. Exiting.')
            sys.exit(1)

# Failover with the HEC library works by passing in a queue to capture failed payloads for processing outside the HEC library
if str(args.failover).lower() == 'true':
    # Create a failure queue
    logger.info('Creating failure queue.')
    fail_q = Queue.Queue(0)

    proc_event = threading.Event()
    proc_event.clear()

    failover_thread_list = []

else:
    # Or not, if we didn't really want it
    fail_q = None
    
'''
Create an object for HEC; assumes raw data send
'''
# Create an HEC object
# Issue - we assume raw but a json endpoint is valid and possibly preferred
splhec_obj = splunk_hec(
    token=args.token,
    hec_server=args.server,
    hec_port=args.port,
    input_type='raw',
    use_hec_tls=True,
    use_ack=args.useack,
    hec_tls_verify=args.tlsverify,
    logger=logger,
    max_threads=HEC_MAX_THREADS,
    backup_queue=fail_q,
    rotate_session_after=HEC_ROTATE_SESSIONS_AFTER,
    max_content_length=HEC_MAX_CONTENT_LENGTH,
    max_events_per_batch=HEC_MAX_BATCH_SIZE,
    context_id=str(args.sourcetype))

# Set params for raw data send
splhec_obj.set_request_params({
    'index': str(args.index),
    'sourcetype': str(args.sourcetype),
    'source': str(args.source),
    'host': str(args.host)
})

'''
Run forever!
'''
run_loop = True
read_list = [sys.stdin]
last_flush = 0
try:
    # Basically, run forever until something happens
    logger.debug('Starting infinity loop.')

    while run_loop and read_list:
        logger.debug('Polling stdin for data.')
        
        if str(args.failover).lower() == 'true':
            current_failover_thread_count = len(failover_thread_list)
            failover_queue_size = fail_q.qsize()
    
            # We only need to do this block of stuff if a failover queue exists        
            if failover_queue_size > 0:
                # No failover threads exist but the queue size is > 0
                if current_failover_thread_count == 0 and failover_queue_size > 0:
                    failover_thread_list.append(start_failover_thread(current_failover_thread_count+1, fail_q, args.dirpath, logger))
                
                # Failover threads exist, but they are fewer than the max amount of threads and data counts are increasing    
                elif current_failover_thread_count < FAILOVER_MAX_THREADS and failover_queue_size > (current_failover_thread_count*3):
                    failover_thread_list.append(start_failover_thread(current_failover_thread_count+1, fail_q, args.dirpath, logger))
                
            # Once failover threads get created, we need to start checking those threads
            if current_failover_thread_count > 0:
                failover_thread_list = do_failure_thread_check(failover_thread_list)
                        

        # Wait up to POLLING_PERIOD seconds for data to be available
        ready = select.select(read_list, [], [], POLLING_PERIOD)[0]

        # If we got no data, we are "idle" and nothing needs to be done except to wait a little bit before polling again.
        if not ready:
            # The point here is to request a flush every X cycles to limit what we're storing in memory in the event of system or application crash.
            # but we really only need to do it during idle time. As long as data is actively flowing in, we can rely on batch settings.
            # 
            if last_flush == 0:
                last_flush = time.time()

            if time.time() - last_flush > MAX_ESTIMATED_SECONDS_BETWEEN_SENDS:
                last_flush = time.time()
                logger.debug('Forcing an event flush because some time has passed.')
                splhec_obj.force_flush_events()

            # During idle time
            time.sleep(POLLING_PERIOD)
        else:
            logger.debug('Data is coming, read it!')
            
            ## Issue: This part of the program should be replaced as data formatting can change.
            
            for file in ready:
                try:
                    # Data input is available, so we grab a line and process it.
                    line = file.readline()
                    
                    if not line:
                        # This is a situation that should never happen unless an EOF is sent by syslog-ng
                        logger.debug('Reached end of file. Flushing.')
                        splhec_obj.force_flush_events()
                        read_list.remove(file)
                        run_loop = False

                    elif line.rstrip():  # optional: skipping empty lines
                        logger.debug('We have data, sending %s to Splunk.', line)
                        splhec_obj.send_event(str(line))
                except:
                    # An exception happened here at one point and the thread died as expected but the unexpected thing
                    # was that syslog-ng didn't restart the process. All other processes were fine except the one that errored out
                    # and it resulted in logs not being accepted.
                    logger.error('Exception while reading line. %s', traceback.format_exc())

except:
    # On exception, kill the loop
    logger.error('Exception caught in run loop. Exception: %s', traceback.format_exc())
    run_loop = False

# when the loop breaks, make sure we clean up splhec_obj to flush out any pending logs
splhec_obj.stop_threads_and_processing()

if fail_q is not None:
    # Just in case, let's make sure a failover thread is active
    failover_thread_list = do_failure_thread_check(failover_thread_list)

    if len(failover_thread_list) == 0:
        failover_thread_list.append(start_failover_thread(current_failover_thread_count+1, fail_q, args.dirpath, logger))
    
    # And then we need to kill off the failover threads
    logger.debug('Blocking until fail_q is empty.')
    fail_q.join()


sys.exit(0)

