#!/usr/bin/env python3
'''
Copyright (C) 2020 John Landers

This program is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
'''

import base64
import time
import json
import queue as Queue
import os

from splunk_hec import splunk_hec


INDEX_DEFINITIONS = {
    'sample-gcp-account-id': {
        'default_index': 'gcp_stackdriver'
    },
    'default': {
        'default_index': 'gcp_stackdriver',
        'linux_os': 'linux',
        'linux_auditd': 'linux_auditd'
    }
}


'''
This is just used to provide normalization of booleans. I use it in just about every script I write.
'''
def normalize_str_to_bool(val):
    val = str(val).lower()
    if val == '1' or val == 'true' or val == 'yes':
        return True
    else:
        return False


def get_index_and_source_for_log(project_id, log_name, text_payload, region=None):
    if project_id in INDEX_DEFINITIONS:
        indexes = INDEX_DEFINITIONS['project_id']
    else:
        indexes = INDEX_DEFINITIONS['default']


    index_type = 'default_index'
    # Set the default log type based on the provided tag which is expected to the be the last item in log name
    if '/' in log_name:
        log_type = log_name.split('/')[-1]
    else:
        log_type = log_name

    # Now convert the log type to "expected" norms for known logs
    if 'syslog' in log_type:
        log_type = '/var/log/messages'
        index_type = 'linux_os'
    elif 'secure-log' in log_type:
        log_type = '/var/log/secure'
        index_type = 'linux_os'
    elif 'audit-log' in log_type:
        log_type = '/var/log/audit/audit.log'
        index_type = 'linux_auditd'

    # Override for audit logs
    if 'audit(' in text_payload:
        log_type = '/var/log/audit/audit.log'

    # Set source based on region, account, and project_id
    if region is None:
        source = 'gcp:%s:%s' % (project_id, log_type)
    else:
        source = 'gcp:%s:%s:%s' % (project_id, region, log_type)

    # Now set an index and return stuffs.
    try:
        return indexes[index_type], source
    except:
        return indexes['default_index'], source


'''
This is the function we're calling on pubsub trigger
'''
def hello_pubsub(event, context):
    """Triggered from a message on a Cloud Pub/Sub topic.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    '''
    Establish a backup queue for handling failover issues
    '''
    splhec.backup_queue = Queue.Queue(0)

    # Update the context id for this invocation
    splhec.context_id = str(context.event_id)


    # Now do some parsing
    payload = {}

    # Decode the pubsub message
    try:
        message = json.loads(base64.b64decode(event['data']).decode('utf-8'))
    except:
        message = base64.b64decode(event['data']).decode('utf-8')

    payload['sourcetype'] = 'gcp:stackdriver'
    payload['host'] = 'unidentified_host'



    # Do a bunch of stuff but only if the JSON conversion worked. Otherwise, move on.
    if type(message) is type({}):
        payload['index'], payload['source'] = get_index_and_source_for_log(message['resource']['labels']['project_id'], message['logName'], message['textPayload'], message['resource']['labels']['zone'] )
        payload['host'] = message['resource']['labels']['instance_id']
        payload['fields'] = { 'gcp_account_id': message['resource']['labels']['project_id'], 'log_group': message['logName'] }

        timestamp = message['timestamp']
        timeformat = '%Y-%m-%dT%H:%M:%SZ'
        try:
            payload['time'] = str(int(time.mktime(time.strptime(str(timestamp), timeformat))))
        except:
            payload['time'] = str(int(time.time()))

        if message['textPayload'].startswith('{'):
            try:
                payload['event'] = json.loads(message['textPayload'])
            except:
                payload['event'] = message['textPayload']

        else:
            payload['event'] = message['textPayload']

    else:
        payload['index'] = INDEX_DEFINITIONS['default']['default_index']
        payload['event'] = message
        payload['host'] = 'error_detected'
        payload['time'] = str(int(time.time()))


    # Add to the queue
    splhec.send_event(json.dumps(payload))

    # But until we figure out something more reasonable, we will have to send the single event out as is
    splhec.force_flush_events()

    # Check the backup q
    if splhec.backup_queue.empty():
            print('No errors detected, moving on.')        
    else:
        # If the backup queue has events, failure happened. This will raise an exception which should force 
        # the background function to retry
        print('Backup queue has events; send failure occurred')

        ### Raise the flag that this message errored and needs to be tried again
        raise RuntimeException('Exiting because logs could not be delivered.')


#################################################################################################################
# Given that an environment can live through multiple executions of the function, it makes some sense to establish all of this
# and then allow the function to be called.

## Need to replicate this concept in AWS.

'''
Get all the environment variables
'''
try:
    hec_server = os.environ['HEC_SERVER']
except KeyError:
    raise KeyError('HEC_SERVER value is not set and must be.')

try:
    hec_token = os.environ['HEC_TOKEN']
except KeyError:
    raise KeyError('HEC_TOKEN value is not set and must be.')

try:
    hec_port = os.environ['HEC_PORT']
except KeyError:
    hec_port = '8088'

try:
    hec_ack = normalize_str_to_bool(os.environ['HEC_ACK'])
except KeyError:
    hec_ack = False        

try:
    hec_ack_attempts = int(os.environ['HEC_ACK_ATTEMPTS'])
except KeyError:
    hec_ack_attempts = 2

try:
    hec_batch_size = int(os.environ['HEC_BATCH_SIZE'])
except KeyError:
    hec_batch_size = 100000

try:
    hec_max_content_length = int(os.environ['HEC_CONTENT_LENGTH'])
except KeyError:
    hec_max_content_length = 75000000



try:
    splhec = splunk_hec(token=hec_token, hec_server=hec_server, hec_port=hec_port, input_type='json', use_hec_tls=True,
        use_ack=hec_ack, hec_tls_verify=False, max_ack_attempts=hec_ack_attempts, max_content_length=hec_max_content_length,
        max_events_per_batch=hec_batch_size, context_id='default', rotate_session_after=100  )

except Exception as ex:
    print('ERROR: Creation of splhec object failed. Exception %s.' % str(ex) )
    raise RuntimeException('Unable to create splhec object. Exit out now.')



#################################################################################################################
