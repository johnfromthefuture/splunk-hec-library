#!/usr/bin/env python3
'''
Copyright (C) 2020 John Landers

This program is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
'''

'''
Version: 1.0.4

    1.0.2 -- Started version tracking. Moved some calls to global scope for efficiency.
    1.0.3 -- Minor memory efficiency improvements based on other lambda improvements
    1.0.4 -- Continued improvements for memory efficiency

##Notes:

1. To allow your function time to process each batch of records, set the source queue's visibility timeout to at least 6 times the timeout 
that you configure on your function. The extra time allows for Lambda to retry if your function execution is throttled while your function is processing a previous batch.

2. Lambda needs the following permissions to manage messages in your Amazon SQS queue. Add them to your function's execution role.
    sqs:ReceiveMessage
    sqs:DeleteMessage
    sqs:GetQueueAttributes

2a. This lambda will also need access to S3 buckets, read access.

3. Make sure that your code throws exceptions if you want to process the message again.
    - SQS should have a deadletter queue to capture messages that can't be delivered.

4. S3 Message Format: https://docs.aws.amazon.com/AmazonS3/latest/dev/notification-content-structure.html

Cloudtrail message format: https://docs.aws.amazon.com/awscloudtrail/latest/userguide/cloudtrail-log-file-examples.html
'''

import json
import os
import boto3
import base64
import gzip
import io
from datetime import datetime

try:
    import Queue
except:
    import queue as Queue

from splunk_hec import splunk_hec

# For specific BU segmentation, defined based on AWS account ID
INDEX_DEFINITIONS_FOR_BUS = {
    'sample-bu-1': 'aws_cloudtrail_bu1',
    'sample-bu-2': 'aws_cloudtrail_bu2'
}

# In some cases, segmentation is enforced based on the data source
INDEX_DEFINITIONS_FOR_SOURCE = {
    's3.amazonaws.com': 'aws_cloudtrail_s3',
    'kms.amazonaws.com': 'aws_cloudtrail_kms',
    'sts.amazonaws.com': 'aws_cloudtrail_sts',
    'ec2.amazonaws.com': 'aws_cloudtrail_ec2',
    'elasticloadbalancing.amazonaws.com': 'aws_cloudtrail_elb',
    'dynamodb.amazonaws.com': 'aws_cloudtrail_database',
    'rds.amazonaws.com': 'aws_cloudtrail_database',
    'athena.amazonaws.com': 'aws_cloudtrail_database',
    'redshift.amazonaws.com': 'aws_cloudtrail_database',
    'elasticmapreduce.amazonaws.com': 'aws_cloudtrail_database',

}

# This is the index that will be used by default or in the event of index assignment error
DEFAULT_INDEX = 'aws_cloudtrail'

# This is used for an override whereby we route monitoring junk from automation to a special index
## THIS DOES NOT APPLY TO INDEXES SET BY BU SEGMENTATION ##
DEV_INDEX = 'aws_cloudtrail_dev'

'''
Used to normalize booleans.
'''
def normalize_str_to_bool(val):
    val = str(val).lower()
    if val == '1' or val == 'true' or val == 'yes':
        return True
    else:
        return False

'''
Just helps make index assignment very clear and separated from the rest of the code
'''
def get_index_for_ct(event_source, aws_account_id=None): 
    try:
        if aws_account_id is not None:
            return INDEX_DEFINITIONS_FOR_BUS[aws_account_id]
    except KeyError:
        pass

    try:
        return INDEX_DEFINITIONS_FOR_SOURCE[event_source]
    except KeyError:
        pass

    return DEFAULT_INDEX


'''
This function should take a JSON dictionary containing our cloudtrail record as an input and return a Splunk HEC /event 
formatted dictionary

'''
def parse_ct_record(ctrecord, bucketname):
    payload = {}
    payload['fields'] = {}
    payload['sourcetype'] = 'aws:cloudtrail'

    # Set the event timestamp.
    #       Sample: 2019-08-13T17:53:31Z
    #       ctrecord['eventTime']
    try:
        payload['time'] = int(datetime.strptime(ctrecord['eventTime'], '%Y-%m-%dT%H:%M:%SZ').strftime('%s'))
    except Exception as ex1:
        print('Unable to convert timeformat for %s. Exception: %s. Defaulting to now.' % (str(ctrecord['eventTime']), str(ex1)))
        payload['time'] = int(time.time())

    # Set the source
    if 'awsRegion' in ctrecord:
        payload['source'] = 'hec:%s:cloudtrail:%s' % (str(bucketname), ctrecord['awsRegion'])
    else:
        payload['source'] = 'hec:%s:cloudtrail' % (str(bucketname))

    ## Set some index-time fields
    try:
        if 'recipientAccountId' in ctrecord:
            payload['host'] = ctrecord['recipientAccountId']
            payload['fields'].update({ 'aws_account_id': ctrecord['recipientAccountId'] })

        if 'eventSource' in ctrecord:
            payload['fields'].update( { 'aws_source_api': ctrecord['eventSource'] } )
    except:
        pass

    ### Set an index for the logs
    try:
        if 'recipientAccountId' in ctrecord:
            payload['index'] = get_index_for_ct(ctrecord['eventSource'], ctrecord['recipientAccountId'])
        else:
            payload['index'] = get_index_for_ct(ctrecord['eventSource'])
        
    except Exception as ex1:
        print('Error setting index. Defaulting to aws_cloudtrail. Exception: %s' % str(ex1))
        #### This is the default index to set on any error
        payload['index'] = DEFAULT_INDEX


    ### Index override for "dev" logs; basically "list", "describe", and "get" events for Redlock, Newrelic, and Datadog

    try:
        if payload['index'] not in list(INDEX_DEFINITIONS_FOR_BUS.values()):
            if str(ctrecord['eventName']).startswith('Get') or str(ctrecord['eventName']).startswith('List') or str(ctrecord['eventName']).startswith('Describe'):
                if 'datadog' in str(ctrecord['userIdentity']['arn']).lower():
                    payload['index'] = DEV_INDEX
                elif 'redlock' in str(ctrecord['userIdentity']['arn']).lower():
                    payload['index'] = DEV_INDEX
                elif 'newrelic' in str(ctrecord['userIdentity']['arn']).lower():
                    payload['index'] = DEV_INDEX
    except:
        pass


    # Do not forget the actual event
    payload['event'] = ctrecord

    return payload

'''
This is the function I expect to be called when the lambda executes. I am expecting that "event" will be the SQS message trigger.
'''
def lambda_handler(event, context):
    '''
    Establish a backup queue for handling failover issues
    '''
    splhec.backup_queue = Queue.Queue(0)
    splhec.context_id = str(context.aws_request_id)

    # Set processing_failed to True to always assume failure
    processing_failed = True

    # We expect a dictionary and if we don't get one, I'm going to assume we were given json in string format.
    if type(event) != type({}):
        event = json.loads(event)

    if 'Records' in event:
        #print('Records key found in SQS message json. Parsing.')
        #print('Received %s records to parse for s3 files.' % len(event['Records']))
        # This construct is meant to help with memory utilization
        while event['Records']:
            record = event['Records'].pop(0)

            mid = str(record['messageId'])
            md5 = str(record['md5OfBody'])
            if type(record['body']) == type({}):
                body = record['body']
            else:
                #print('Provided message body does not appear to be a json dictionary. Attempting conversion but this may not work as expected.')
                body = json.loads(record['body'])

            #print('Parsing message id %s with md5 %s.' % (mid, md5))
            #print('Message body: %s' % json.dumps(body))

            if 'Records' in body:
                #print( 'Received %s S3 records to parse within SQS message ID %s.' % (len(body['Records']),mid) )
                while body['Records']:
                    s3record = body['Records'].pop(0)
                    try:
                        s3item = s3record['s3']
                        bucket_name = str(s3item['bucket']['name'])
                        object_path = str(s3item['object']['key'])
                        object_size = int(s3item['object']['size'])

                    except:
                        print('ERROR: S3 record does not match expected formatting; unable to get bucket name and object path.')
                    
                    else:
                        if '/CloudTrail-Digest/' in str(object_path):
                            #print('Provided object path is a digest file. Ignoring.')

                            # Set this because processing has not failed; the file is being ignored successfully
                            processing_failed = False

                        else:
                            #params = { 'source' : ('hec:%s:cloudtrail' % (str(bucket_name))) }

                            #splhec.set_request_params( params )
                            #print('Creating connection to S3 bucket %s' % bucket_name)
                            s3obj = boto3.client('s3')
    

                            # While working with files I noted some 3-4mb gzip files expanding up to 42mb, and these seemed to fail when trying to parse
                            # so I added this logic to enforce a line-by-line method for files larger than 1mb. The limitation is believed
                            # to be in memory allocation relative to the gzip library. 
                            if object_size < 1000000:
                                print('context=%s file_size=%s path=%s' % (str(context.aws_request_id), str(object_size), 'memory_only'))
                                # Do the whole thing in memory

                                # memory buffer for downloading the file
                                bytes_buffer = io.BytesIO()
                                
                                # download file to mem
                                s3obj.download_fileobj(Bucket=bucket_name, Key=object_path, Fileobj=bytes_buffer)

                                # Get the byte value
                                byte_value = bytes_buffer.getvalue()

                                # After getting the value, the buffer is unnecessary and should be cleared from memory.
                                bytes_buffer = None

                                # Put the string data in memory for further parsing
                                try:
                                    file_data = gzip.decompress(byte_value).decode('utf-8','ignore')
                                except:
                                    # Convert to string
                                    file_data = byte_value.decode('utf-8', 'ignore')

                                # After decoding/unzipping, we should clear the byte_value
                                byte_value = None


                            else:
                                print('context=%s file_size=%s path=%s' % (str(context.aws_request_id), str(object_size), 'tmp_file_download'))

                                tmp_file_name = '/tmp/' + str(os.path.basename(object_path))
                                #print('Opening temporary file: %s' % str(tmp_file_name))
        
                                with open(tmp_file_name, 'wb') as f:
                                    #print('Downloading file: %s' % object_path )
                                    s3obj.download_fileobj(bucket_name, object_path, f)
        
                                try:
                                    file_data = ''
                                    with gzip.open(tmp_file_name, 'rb') as f:
                                        for line in f:
                                            file_data+=line.decode('utf-8', 'ignore')
                                except OSError:
                                    file_data = ''
                                    with open(tmp_file_name, 'r') as f:
                                        for line in f:
                                            file_data+=line
                                except Exception as ex1:
                                    print('Exception: %s' % str(ex1))
                                    raise RuntimeError('Failure to download and open the file.')

                                try:
                                    os.remove(tmp_file_name)
                                    #print('Temp file deleted.')
                                except:
                                    #print('Exception occurred while attempting to delete the temp file. Ignoring.')
                                    pass

                              
                            # No matter what, we are expecting to parse the whole file in memory because it's a big json blob
                            try:
                                file_data = json.loads(file_data)


                            except Exception as ex:
                                print('ERROR: Exception caught while trying to parse CT data. Exception: %s' % str(ex))
                                raise RuntimeError('Failure to parse data, exiting immediately.')
    
                            if 'Records' in file_data:
                                print('Lambda measurement:: context=%s parsed_file=%s record_count=%s' % (str(context.aws_request_id),str(object_path),str(len(file_data['Records']))))
                                while file_data['Records']:
                                    ctrecord = file_data['Records'].pop(0)      

                                    #### Parse the ctrecord which should be a json library ####
                                    #### Parsing should determine the index
                                    payload = parse_ct_record(ctrecord, str(bucket_name))

                                    # Send the payload which should be the fully parsed event
                                    splhec.send_event(json.dumps(payload, sort_keys=True))
    
                                # We'll set this to false here because at this point, we can assume that our processing went to plan
                                processing_failed = False
    
                            else:
                                print('ERROR: Data does not contain records key. This is unexpected. Record: %s' % str(json.dumps(file_data)))
                                raise RuntimeError('Something is wrong with the parsed file. Review required. Object path: %s' % str(object_path))
    
                            
                            # I'm doing this because I want to flush out events before moving to a new S3 object
                            splhec.force_flush_events()

                            # And we should check for failures and stop processing if we see them.
                            if splhec.backup_queue.empty():
                                #print('Backup queue appears to be empty. Continuing execution.')
                                pass
                            else:
                                print('ERROR: Backup queue is not empty. Raising runtime error.')
                                raise RuntimeError('Delivery failures detected. Raising exception to make the lambda error out and not delete the SQS message.')


            else:
                print('ERROR: Message does not match expected format. Body of message id %s does not contain Records key. Body: %s' % (mid, str(json.dumps(body))))
                # We get a number of messages that are tests or do not actually provide an s3 path so
                # I don't want to reprocess those. We'll print the body out for good measure but otherwise, consider processing successful.
                processing_failed = False
    else:
        print('ERROR: Event passed to lambda function does not match expectations. Does not contain Records key. Event: %s' % str(json.dumps(event)))


    # At the end of the function call, we need to clear out the data still pending
    splhec.force_flush_events()
    
    #print('Performing a final check on the backup_q for failures.')

    if splhec.backup_queue.empty():
        #print('Execution appears successful.')
        raise_error = False
    else:
        print('ERROR: Backup queue is not empty. Raising runtime error.')
        raise_error = True


    if processing_failed or raise_error:
        raise RuntimeError('Processing failed. Raising an exception so the lambda is seen as failed and the SQS message will be returned to the queue.')
        return {
            'statusCode': 500,
            'body': json.dumps('Processing failed. Raising an exception so the lambda is seen as failed and the SQS message will be returned to the queue.')
        }
    else:
        return {
            'statusCode': 200,
            'body': json.dumps('Processing all messages succeeded.')
        }


### Making this global as it is unnecessary to reinstatiate on every request.
try:
    hec_server = os.environ['HEC_SERVER']
except KeyError:
    raise KeyError('HEC_SERVER value is not set and must be.')

try:
    hec_token = os.environ['HEC_TOKEN']
except KeyError:
    raise KeyError('HEC_TOKEN value is not set and must be.')

try:
    hec_port = os.environ['HEC_PORT']
except KeyError:
    hec_port = '8088'

try:
    hec_ack = normalize_str_to_bool(os.environ['HEC_ACK'])
except KeyError:
    hec_ack = False        

try:
    hec_ack_attempts = int(os.environ['HEC_ACK_ATTEMPTS'])
except KeyError:
    hec_ack_attempts = 5

try:
    hec_batch_size = int(os.environ['HEC_BATCH_SIZE'])
except KeyError:
    hec_batch_size = 100000

try:
    hec_max_content_length = int(os.environ['HEC_CONTENT_LENGTH'])
except KeyError:
    hec_max_content_length = 50000000


# Create an splhec object
try:
    splhec = splunk_hec(token=hec_token, hec_server=hec_server, hec_port=hec_port, input_type='json', use_hec_tls=True,
        use_ack=hec_ack, hec_tls_verify=False, max_ack_attempts=hec_ack_attempts, max_content_length=hec_max_content_length,
        max_events_per_batch=hec_batch_size )

except Exception as ex:
    print('ERROR: Creation of splhec object failed. Exception %s.' % str(ex) )
    raise RuntimeException('Unable to create splhec object. Exit out now.')
