#!/usr/bin/env python3
'''
Copyright (C) 2020 John Landers

This program is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
'''


'''
This lambda is used for Cloudwatch Logs and VPC Flows; be advised that index routing is somewhat complex so do a full code review before you try to implement this in your environment.



Description: This lambda should trigger off a kinesis stream and deliver those events to the target Splunk HEC

Lambda requires the following permissions to be a stream subscriber:
    kinesis:DescribeStream
    kinesis:DescribeStreamSummary
    kinesis:GetRecords
    kinesis:GetShardIterator
    kinesis:ListShards
    kinesis:ListStreams
    kinesis:SubscribeToShard


Environment variables supported:
    
    HEC_SERVER
        Default: None
        Required: True
        Value: FQDN for the HEC server/LB

    HEC_TOKEN
        Default: None
        Required: True
        Value: HEC Token allowing access to send data to any index

    HEC_PORT
        Default: 8088
        Required: False
        Value: Port number

    HEC_ACK
        Default: False
        Required: False
        Value: True/False - Determines if ACK is used when sending data. Will increase invocation time drastically if utilized.

    HEC_ACK_ATTEMPTS
        Default: 2
        Required: False
        Value: Integer - Number of times we should try to send data and number of times we should try to ack that data was received

    S3_BUCKET_NAME
        Default: None
        Required: False (Highly Recommended)
        Value: Bucket name to store failover files. Used to store logs when a failure to deliver to HEC is detected and not corrected.

    S3_FILE_PREFIX
        Default: cwl/lambda/
        Required: False
        Value: Prefix key in the S3 bucket where files are stored

    HEC_BATCH_SIZE
        Default: 100000
        Required: False
        Value: Integer - Maximum number of events to send in a single post to the HEC

    HEC_CONTENT_LENGTH
        Default: 75000000
        Required: False
        Value: Integer - Maximum estimated payload length to send in a single post to HEC. Should not exceed 800MB.

    EXPECTED_LOG_TYPE
        Default: cwl
        Required: False
        Value: 
                cwl -- for Cloudwatch Logs generic processing
                vpc -- for VPC Flow Logs via CWL - will convert to JSON


To-Do:
    * Move index definitions to a separate file in an easy-to-maintain format.

'''

import base64
import json
import gzip
import os
import re

try:
    import Queue
except:
    import queue as Queue

from splunk_hec import splunk_hec

'''
This is used to define index routing. It's essentially a dictionary broken out like so:
  { 
     'aws_account_id': { 'default_index': 'index_name' }
  }

The use of a 'default_index' is 100% mandatory or logs will not be routed appropriately. If we try to get an index that was not defined
for the given BU, we will instead use the default_index. If the default_index is not defined, we'll use the default_index from the default key.

For good measure, linux_auditd and linux_os should also exist to handle auditd index overrides that occur later.
'''
INDEX_DEFINITIONS={
    'sample-aws-account-id': {
        'default_index': 'bu_index',
        'default_app': 'bu_index_app',
        'default_web': 'bu_index_web'
    },
    'default': {
        'default_index': 'aws_cloudwatchlogs',
        'default_app': 'app',
        'default_web': 'web',
        'default_email': 'email',
        'default_database': 'database',
        'dev': 'devlogs',
        'windows_os': 'wineventlog',
        'linux_os': 'linux',
        'vormetric': 'vormetric_dsm',
        'mssql': 'mssql',
        'cassandra': 'cassandra',
        'aurora': 'aws_aurora',
        'linux_auditd': 'linux_auditd',
        'lambda': 'aws_lambda',
        'vpcflows': 'aws_vpcflows'
    }
}

'''
This is just used to provide normalization of booleans. I use it in just about every script I write.
'''
def normalize_str_to_bool(val):
    val = str(val).lower()
    if val == '1' or val == 'true' or val == 'yes':
        return True
    else:
        return False

'''
Everything after this point are functions supporting the lambda handler function or the handler function itself.
'''
######################################

'''
for returning appropriate index definitions
'''
def get_index_definitions(aws_account_id):
    try:
        if aws_account_id in INDEX_DEFINITIONS:
            defs=INDEX_DEFINITIONS[aws_account_id]
            if 'default_index' not in defs:
                defs['default_index'] = INDEX_DEFINITIONS['default']['default_index']

            return defs
        else:
            return INDEX_DEFINITIONS['default']
    except:
        return INDEX_DEFINITIONS['default']

'''
For deciding on the destination index based on log group.
'''
def get_index_for_log(log_group, aws_account_id=None):
    # Step 1: Get the right index definitions
    definitions = get_index_definitions(aws_account_id)

    # Step 2: Find the index
    # This list of index modifications comes from the current
    # transformations being applied at index time
    try:
        if 'Windows' in log_group:
            return definitions['windows_os']
        elif 'MSSQL' in log_group:
            return definitions['mssql']
        elif 'cassandra' in log_group:
            return definitions['cassandra']
        elif '/aws/rds/' in log_group:
            return definitions['aurora']
        elif 'audit/audit.log' in log_group:
            ##### NOTE: THIS IS ALSO CHECKED ELSEWHERE DUE TO INCORRECT LOG GROUPS BEING SENT TO US #####
            return definitions['linux_auditd']
        elif 'var/log/github' in log_group:
            return definitions['default_app']
        elif 'aws/lambda' in log_group:
            return definitions['lambda']
        elif 'haproxy' in log_group:
            return definitions['default_web']
        elif 'squid' in log_group:
            return definitions['default_web']
        elif 'nginx' in log_group:
            return definitions['default_web']
        elif '/var/log/maillog' in log_group:
            return definitions['default_email']
        elif 'MongoDB' in log_group:
            return definitions['default_database']
        elif 'mysql-logs' in log_group:
            return definitions['default_database']
        elif 'var/log/' in log_group:
            # logs so needs to be towards the end of the if/elif chain
            return definitions['linux_os']
    except:
        # If there's a failure of any kind, we still need to set an index
        return definitions['default_index']


    # Failover to cloudwatchlogs
    return definitions['default_index']

'''
For getting the host name, generally based on log stream, but sometimes based on log group.
'''
def get_host_for_log(log_group, log_stream):
    # We ask business units to send logs via CWL with the structure: <whatever they want>_<instanceid>
    # and this extracts that to set a host.

    # We need to differentiate based on log_group, primarily for lambdas where the log_group is a better source
    # for the name of the host than log_stream.
    try:
        if 'aws/lambda' in log_group:
            extracted_host = str(re.search('aws/lambda/(.+)',log_group).group(1))
        else:
            extracted_host = str(re.search('^([^\s\_\.\/]+)',log_stream).group(1))
        
        # If a match was not returned previously, an exception would occur
        # so I'm not testing much here before return.
        return extracted_host

    except Exception as ex:
        print('ERROR: Exception while trying to extract host name. Setting default. :: %s' % str(ex))
        return 'undetermined_host'


'''
This function is for parsing generic CWL logs and adding it to the HEC send queue
'''

def cwl_parse_log_event(entry, splhec):
    print('context=%s, Record metadata. msg_log_group=%s, msg_log_stream=%s, msg_log_owner=%s, count_entries=%s' % (str(splhec.context_id),str(entry['logGroup']),str(entry['logStream']),str(entry['owner']),str(len(entry['logEvents']))))
    # This was done because *normally* these values will not change
    # But there is the potential for over-ride
    index_definitions = get_index_definitions(str(entry['owner']))
    planned_index = get_index_for_log(str(entry['logGroup']), str(entry['owner']))
    planned_source = 'cwl:%s:%s:%s' % (str(entry['owner']), str(entry['aws_region']), str(entry['logGroup']))
    planned_host = get_host_for_log(str(entry['logGroup']), str(entry['logStream']))
    field_extractions = { 'aws_account_id': str(entry['owner']), 'log_stream': str(entry['logStream']), 'orig_log_group': str(entry['logGroup']) }
    planned_sourcetype = 'aws:cloudwatchlogs:lambda'

    override_counter_check = 0
    override_counter = 0

    try:
        if planned_index is not index_definitions['linux_auditd'] and planned_index is index_definitions['linux_os'] and 'audit.log' not in planned_source:    
            do_override_check = True
        else:
            do_override_check = False
    except Exception as ex5:
        print('Exception occurred while trying to check for the necessity of an override. %s' % str(ex5))
        do_override_check = False

    # Roll through each log entry and format appropriately
    while entry['logEvents']:
        single_log = entry['logEvents'].pop(0)
        # Construct the payload to send to HEC
        payload = {}
        payload['index'] = planned_index
        payload['source'] = planned_source
        payload['sourcetype'] = planned_sourcetype
        payload['host'] = planned_host

        # Due to a later use of dict.update, I put this here to ensure it was assigned fresh on every pass
        # otherwise the "orig_log_group" field ends up in every message from an account with an override.
        payload['fields'] = field_extractions

        #### Do an override of the source/index based on identifying auditd logging ###
        if do_override_check:
            override_counter_check+=1
            try:
                if 'audit(' in str(single_log['message']): 
                    override_counter+=1
                    payload['source'] = 'cwl:%s:%s:%s' % (str(entry['owner']), str(entry['aws_region']), '/var/log/audit/audit.log')
                    payload['index'] = index_definitions['linux_auditd']

            except Exception:
                pass

        # Adding a quick timestamp conversion for splunkyness
        if len(str(single_log['timestamp']))>10:
            payload['time'] = str(int(int(single_log['timestamp'])/1000))
        else:
            payload['time'] = single_log['timestamp']

        # str(single_log['message'])
        # In the event that we get json messages, we should try to format
        # them as such. This does a very simple test for that.
        if str(single_log['message']).startswith('{'):
            try:
                # Try to load the message as a json dictionary
                payload['event'] = json.loads(str(single_log['message']))
            except:
                # And if that fails, just default to the original plan
                payload['event'] = str(single_log['message'])
        else:
            # If the event didn't start with a {, just do the usual thing
            payload['event'] = str(single_log['message'])

        # Send the event
        splhec.send_event(json.dumps(payload))
        
    print('context=%s, Override counter stats. override_checked=%s, override_applied=%s' % (str(splhec.context_id), str(override_counter_check), str(override_counter) ))

'''
for parsing vpc events
'''
def vpc_parse_log_event(entry, splhec):
    # This is being removed to reduce the amount of stuff in memory at any given time. The events are already in memory
    #events = [ ev['message'].split(' ') for ev in entry['logEvents'] if ev['message'].endswith('OK') ]

    index_definitions = get_index_definitions(str(entry['owner']))

    # Set up the payload with the values that do not change for a single entry dictionary
    payload = {}
    payload['source'] = 'aws:vpcflows:' + str(entry['owner']) + ':' + str(entry['aws_region'])
    payload['sourcetype'] = 'aws:cloudwatchlogs:vpcflow'


    try:
        if 'vpcflows' in index_definitions:
            payload['index'] = index_definitions['vpcflows']
        elif 'default_index' in index_definitions:
            payload['index'] = index_definitions['default_index']
        else:
            # Something went really wrong here I think.
            payload['index'] = 'aws_vpcflows'

            try:
                print('DEBUG: ERROR: aws_account_id=%s' % entry['owner'])
                print('DEBUG: ERROR: index_definitions=%s' % json.dumps(index_definitions))

            except:
                pass
    except Exception as ex:
        #print('DEBUG: ERROR: aws_account_id=%s' % entry['owner'])
        #if index_definitions is not None:
        #    print('DEBUG: ERROR: index_definitions=%s' % json.dumps(index_definitions))
        raise RuntimeError('Unable to set an index for vpcflows. Exception: %s' % str(ex))


    payload['fields'] = {}
    payload['fields']['aws_account_id'] =  str(entry['owner'])

    # Loop each log event in the given entry dictionary
    while entry['logEvents']:
        event = entry['logEvents'].pop(0)
        # Only parse the event if it ends with OK; ignore NODATA/SKIPDATA events.
        if event['message'].endswith('OK'):

            # Log is space delimited; split by a space
            msg = event['message'].split(' ')

            # Set up the return data
            return_data = {}
            return_data['version'] = msg[0]
            return_data['account_id'] = msg[1]
            return_data['interface_id'] = msg[2]
            return_data['src_ip'] = msg[3]
            return_data['dest_ip'] = msg[4]
            return_data['src_port'] = msg[5]
            return_data['dest_port'] = msg[6]
            return_data['protocol_code'] = msg[7]
            return_data['packets'] = msg[8]
            return_data['bytes'] = msg[9]
            return_data['start_time'] = msg[10]
            return_data['end_time'] = msg[11]
            return_data['vpcflow_action'] = msg[12]
            return_data['log_status'] = msg[13]

            # Update payload time
            payload['time'] = msg[10]
                    
            # update payload host
            if msg[2] != '-':
                payload['host'] = msg[2]
            else:
                payload['host'] = 'unspecified'
                    
            # update payload event data
            payload['event'] = return_data

            # Send it to the HEC
            splhec.send_event(json.dumps(payload))

'''
Lambda handler function
'''
def handler(event, context):
    '''
    Establish a backup queue for handling failover issues
    '''
    splhec.backup_queue = Queue.Queue(0)
    splhec.context_id = str(context.aws_request_id)

    '''
    Make sure 'Records' exist in the provided event.
    '''
    # Documentation implies that a valid message will have the Records key
    if 'Records' in event:
        # For measurement purposes.
        event_count = 0
        data_messages = 0

        # Loop through each record in the event
        while event['Records']:
            r = event['Records'].pop(0)
            # Decode the data
            if 'kinesis' in r:
                data = base64.b64decode(r['kinesis']['data'])
            else:
                data = None   

            if data is not None:
                # Decompress the data
                data = json.loads(gzip.decompress(data))

                ## Now we are left with the CWL message...
                if data['messageType'] == 'DATA_MESSAGE':
                    data_messages += 1
                    #print('Message type is DATA_MESSAGE. Will parse.')
                    try:
                        ## Basically, I'm copying the region from the kinesis metadata to the CWL Metadata for later use 
                        data['aws_region'] = r['awsRegion']
                    except:
                        # And if that fails for some reason, grab the region from the lambda environment...
                        data['aws_region'] = str(os.environ['AWS_REGION']).lower()

                    # Increment the event count by the length of the logEvent list we expect to see
                    event_count += len(data['logEvents'])

                    # Send the data dictionary over to the correct parser function
                    if 'vpc' in expected_parser_type:
                        vpc_parse_log_event( data, splhec )
                    else:
                        cwl_parse_log_event( data, splhec )


                elif data['messageType'] == 'CONTROL_MESSAGE':
                    #print('Message type is CONTROL_MESSAGE. Ignored.')
                    pass
                else:
                    print('ERROR: Other message type %s. Ignored.' % str(data['messageType']))

            else:
                # If a given message doesn't follow our expected format it gets ignored so if we see a lot of these errors, it needs investigation
                print('Provided record did not have the key "kinesis" which is unexpected. Record was not parsed. Original record: %s ' % str(r))

        # Flush any queued/batched events
        splhec.force_flush_events()

        '''
        Main execution complete. Check for failures, print measurement statements.
        '''
        print('CWL lambda measurement:: context=%s num_total_records=%s num_cwl_data_messages=%s total_events_seen=%s' % ( str(context.aws_request_id), str(len(event['Records'])), str(data_messages),  str(event_count) ))
        if splhec.backup_queue.empty():
            #print('No errors detected, moving on.')
            return {
                'statusCode': 200,
                'body': json.dumps('Processing all messages succeeded.')
            }
        else:
            # Let's import these only when they are needed.
            import time
            import hashlib
            import boto3
            print('ERROR: CWL processing resulting in %s delivery errors.' % (str(splhec.backup_queue.qsize())) )
            
            ### At this point, we have failures, we're going to take those and put them into files and upload to S3
            ### so that if the HEC were to completely die, we can still store files

            print('Creating connection to S3 bucket %s' % bucket_name)
            s3obj = boto3.client('s3')

            clear_failover_queue = True
            events_to_s3 = []

            while clear_failover_queue:
                if splhec.backup_queue.empty():
                    clear_failover_queue = False
                    break

                try:
                    #print('Attempting to get data from the failover queue.')
                    payload = splhec.backup_queue.get(False)
                except:
                    #print('No data exists.')
                    pass

                else:
                    #print('Data found. Adding to list that will be sent to S3 later.')
                    events_to_s3.append(payload)
                    splhec.backup_queue.task_done()


            try:                
                data_body = '\n'.join(events_to_s3)
                if s3_prefix.endswith('/'):
                    data_key = s3_prefix + str(time.strftime('%Y/%m/%d/%H')) + '/' + str(hashlib.md5(str(data_body).encode("utf-8")).hexdigest()) + '.log'
                else:
                    data_key = s3_prefix + '/' + str(time.strftime('%Y/%m/%d/%H')) + '/' + str(hashlib.md5(str(data_body).encode("utf-8")).hexdigest()) + '.log'

                #print('Attempting to write data to S3.')
                s3obj.put_object(Bucket=bucket_name, Key=data_key, Body=data_body)

                print('Failover succeeded. File write to s3://%s/%s' % ( str(bucket_name), str(data_key) ) )

                return {
                    'statusCode': 200,
                    'body': json.dumps('Data processed and stored in S3.')
                }   

            except Exception as ex:
                raise RuntimeError('Processing failed. Unable to failover to S3. Exception: %s.' % str(ex))
                return {
                    'statusCode': 500,
                    'body': json.dumps('Processing failed. Raising an exception so the lambda is seen as failed.')
                }   


    else:
        # This event is invalid from our expectations so it gets ignored.
        print('ERROR: Invalid event passed into the lambda. Original event: %s.' % str(json.dumps(event)))
        return {
            'statusCode': 200,
            'body': json.dumps('Invalid message was sent but setting status 200 because this should not require parsing again.')
        }


    return {
        'statusCode': 200,
        'body': json.dumps('Default return message. No other return happened.')
    }




#####################################
'''
Globalized variables to prevent reassignment on every function call. Cold boot invocations will have to set these but
follow on invocations should be able to benefit from having them already set saving a millisecond or two. However, we should
not modify these during function invocation.
'''
try:
    hec_server = os.environ['HEC_SERVER']
except KeyError:
    raise KeyError('HEC_SERVER value is not set and must be.')

try:
    hec_token = os.environ['HEC_TOKEN']
except KeyError:
    raise KeyError('HEC_TOKEN value is not set and must be.')

try:
    hec_port = os.environ['HEC_PORT']
except KeyError:
    hec_port = '8088'

try:
    hec_ack = normalize_str_to_bool(os.environ['HEC_ACK'])
except KeyError:
    hec_ack = False        

try:
    hec_ack_attempts = int(os.environ['HEC_ACK_ATTEMPTS'])
except KeyError:
    hec_ack_attempts = 5

try:
    bucket_name = str(os.environ['S3_BUCKET_NAME'])
except KeyError:
    raise KeyError('You must set a S3_BUCKET_NAME environment variable for failover.')

try:
    s3_prefix = str(os.environ['S3_FILE_PREFIX'])
except KeyError:
    s3_prefix = 'cwl/lambda/'

try:
    hec_batch_size = int(os.environ['HEC_BATCH_SIZE'])
except KeyError:
    hec_batch_size = 100000

try:
    hec_max_content_length = int(os.environ['HEC_CONTENT_LENGTH'])
except KeyError:
    hec_max_content_length = 75000000

try:
    expected_parser_type = str(os.environ['EXPECTED_LOG_TYPE']).lower()
except KeyError:
    expected_parser_type = 'cwl'


'''
Create an object for communicating to HEC
'''
try:
    splhec = splunk_hec(token=hec_token, hec_server=hec_server, hec_port=hec_port, input_type='json', use_hec_tls=True,
        use_ack=hec_ack, hec_tls_verify=False, max_ack_attempts=hec_ack_attempts, max_content_length=hec_max_content_length,
        max_events_per_batch=hec_batch_size )

except Exception as ex:
    print('ERROR: Creation of splhec object failed. Exception %s.' % str(ex) )
    raise RuntimeError('Unable to create splhec object. Exit out now.')

