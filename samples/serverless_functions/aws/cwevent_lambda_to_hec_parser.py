#!/usr/bin/env python3

'''
Copyright (C) 2020 John Landers

This program is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
'''

'''
This function is for handling Cloudwatch Events. Typical pipeline: Cloudwatch Events > Event Bus > Lambda


Description: This lambda should trigger off a kinesis stream and deliver the resulting CW events to the target Splunk HEC

Lambda requires the following permissions to be a stream subscriber:
    kinesis:DescribeStream
    kinesis:DescribeStreamSummary
    kinesis:GetRecords
    kinesis:GetShardIterator
    kinesis:ListShards
    kinesis:ListStreams
    kinesis:SubscribeToShard

Environment variables supported:
    
    HEC_SERVER
        Default: None
        Required: True
        Value: FQDN for the HEC server/LB

    HEC_TOKEN
        Default: None
        Required: True
        Value: HEC Token allowing access to send data to any index

    HEC_PORT
        Default: 8088
        Required: False
        Value: Port number

    HEC_ACK
        Default: False
        Required: False
        Value: True/False - Determines if ACK is used when sending data. Will increase invocation time drastically if utilized.

    HEC_ACK_ATTEMPTS
        Default: 2
        Required: False
        Value: Integer - Number of times we should try to send data and number of times we should try to ack that data was received

    S3_BUCKET_NAME
        Default: None
        Required: False (Highly Recommended)
        Value: Bucket name to store failover files. Used to store logs when a failure to deliver to HEC is detected and not corrected.

    S3_FILE_PREFIX
        Default: cwl/lambda/
        Required: False
        Value: Prefix key in the S3 bucket where files are stored

    HEC_BATCH_SIZE
        Default: 100000
        Required: False
        Value: Integer - Maximum number of events to send in a single post to the HEC

    HEC_CONTENT_LENGTH
        Default: 75000000
        Required: False
        Value: Integer - Maximum estimated payload length to send in a single post to HEC. Should not exceed 800MB

'''
import base64
import json
import gzip
import os
import re
import time

try:
    import Queue
except:
    import queue as Queue

from splunk_hec import splunk_hec

INDEX_DEFINITIONS = {
    'sample-aws-account-id':  { 
        'cloudtrail': 'main', 
        'default_index': 'main'
    },
    'default':  { 
        'cloudtrail': 'aws_cloudtrail',
        'guardduty': 'aws_guardduty',
        'aws_config': 'aws_config',
        'default_index': 'aws_cloudwatch_events' 
    },
}

'''
This is just used to provide normalization of booleans. I use it in just about every script I write.
'''
def normalize_str_to_bool(val):
    val = str(val).lower()
    if val == '1' or val == 'true' or val == 'yes':
        return True
    else:
        return False

##################
'''
Parse environment variables for use with the lambda
'''
try:
    hec_server = os.environ['HEC_SERVER']
except KeyError:
    raise KeyError('HEC_SERVER value is not set and must be.')

try:
    hec_token = os.environ['HEC_TOKEN']
except KeyError:
    raise KeyError('HEC_TOKEN value is not set and must be.')

try:
    hec_port = os.environ['HEC_PORT']
except KeyError:
    hec_port = '8088'

try:
    hec_ack = normalize_str_to_bool(os.environ['HEC_ACK'])
except KeyError:
    hec_ack = False        

try:
    hec_ack_attempts = int(os.environ['HEC_ACK_ATTEMPTS'])
except KeyError:
    hec_ack_attempts = 5

try:
    bucket_name = str(os.environ['S3_BUCKET_NAME'])
except KeyError:
    raise KeyError('You must set a S3_BUCKET_NAME environment variable for failover.')

try:
    s3_prefix = str(os.environ['S3_FILE_PREFIX'])
except KeyError:
    s3_prefix = 'cwevents/'

try:
    hec_batch_size = int(os.environ['HEC_BATCH_SIZE'])
except KeyError:
    hec_batch_size = 100000

try:
    hec_max_content_length = int(os.environ['HEC_CONTENT_LENGTH'])
except KeyError:
    hec_max_content_length = 75000000



'''
Create an object for communicating to HEC
'''
try:
    splhec = splunk_hec(token=hec_token, hec_server=hec_server, hec_port=hec_port, input_type='json', use_hec_tls=True,
        use_ack=hec_ack, hec_tls_verify=False, max_ack_attempts=hec_ack_attempts, max_content_length=hec_max_content_length,
        max_events_per_batch=hec_batch_size )

except Exception as ex:
    print('ERROR: Creation of splhec object failed. Exception %s.' % str(ex) )
    raise RuntimeException('Unable to create splhec object. Exit out now.')

#################

'''
Determine the index for the event
'''
def get_index_for_cwevent(account_id, sourcetype):
    account_id = str(account_id).lower()

    if account_id in INDEX_DEFINITIONS:
        indexes = INDEX_DEFINITIONS[account_id]
    else:
        indexes = INDEX_DEFINITIONS['default']


    try:
        if 'cloudtrail' in sourcetype:
            return indexes['cloudtrail']
        elif 'guardduty' in sourcetype:
            return indexes['guardduty']
        elif 'config' in sourcetype:
            return indexes['aws_config']
        elif 'trustedadvisor' in sourcetype:
            return indexes['trustedadvisor']
        else:
            return indexes['default_index']

    except:
        # We expect a KeyError when we try to return a key that doesn't exist
        # and when that happens, return the default index
        return indexes['default_index']


'''
Determines the sourcetype for the given event
'''
def get_sourcetype_for_cwevent( detail_type, source ):
    detail_type = str(detail_type).lower()
    source = str(source).lower()

    # Any errors and we'll return the default sourcetype
    try:
        if 'cloudtrail' in detail_type:
            return 'aws:cwe:cloudtrail'
        elif 'guardduty' in detail_type:
            return 'aws:cwe:guardduty'
        elif 'aws.config' in source:
            return 'aws:config'
        elif 'aws.trustedadvisor' in source:
            return 'aws:trustedadvisor'
        else:
            # This attempts to create a more dynamic sourcetype based on the data source being sent
            sourcetype = re.sub('[^0-9a-zA-Z]+',':', source)
            if 'aws:' in sourcetype:
                return sourcetype
            else:
                return 'aws:' + sourcetype
    except:
         return 'aws:cwe:generic'

    # Default
    return 'aws:cwe:generic'


def parse_cwevent_message( payload, splhec ):
    # We expect, after base64 encoding is removed, the payload is a json dictionary
    try:
        payload = json.loads(payload)
    except:
        raise RuntimeError('Unable to load payload as JSON. This is unexpected. Payload: %s' % str(payload))

    # This time pattern came from a CWEvent sample and is assumed to be universally true for this event format
    time_pattern = "%Y-%m-%dT%H:%M:%SZ"


    splunk_event = {}
    splunk_event['fields'] = {}

    # Set a default source
    splunk_event['source'] = 'aws:cwe:default'

    # Try setting a time for the event
    try:
        splunk_event['time'] = str(int(time.mktime(time.strptime(str(payload['time']), time_pattern))))
    except KeyError:
        # Default to now because a time field wasn't provided
        splunk_event['time'] = str(int(time.time()))
    except:
        splunk_event['time'] = str(int(time.time()))
        # Something happend and we couldn't parse the time. Default to now.
        print('Unable to parse time field in original event. Original: %s; Format Given: %s. Defaulting to now.' % (str(payload['time']), str(time_pattern) ) )
    
    # Set up some indexed fields
    if 'account' in payload:
        splunk_event['fields'].update( { 'aws_account_id': payload['account'] } )
        splunk_event['host'] = payload['account']

    if 'region' in payload:
        splunk_event['fields'].update( { 'aws_region': payload['region'] } )

    # Try making the source better
    try:
        splunk_event['source'] = 'aws:cwe:%s:%s' % (str(payload['region']), str(payload['account']))
    except:
        # Do nothing on failure, just use the default source
        pass

    # Set the sourcetype
    splunk_event['sourcetype'] = get_sourcetype_for_cwevent(source=payload['source'], detail_type=payload['detail-type'])

    # Set the index based on a number of potential factors
    splunk_event['index'] = get_index_for_cwevent(account_id=payload['account'], sourcetype=splunk_event['sourcetype'] )

    # Finally, add the event itself
    if 'detail' in payload:
        splunk_event['event'] = payload['detail']
    else:
        print('Provided event does not have a detail key which is unexpected. Putting the whole payload in the new_event event key.')
        splunk_event['event'] = payload

    # And send it to the HEC queue
    splhec.send_event(json.dumps(splunk_event))

'''
lambda handler function
'''
def handler(event, context):
    '''
    Establish a backup queue for handling failover issues
    '''
    splhec.backup_queue = Queue.Queue(0)
    splhec.context_id = str(context.aws_request_id)


    '''
    Make sure 'Records' exist in the provided event.
    '''
    if 'Records' in event:
        # For measurement purposes.
        event_count = 0

        # Loop through each record in the event
        for r in event['Records']:
            # Decode the data
            if 'kinesis' in r:
                data = base64.b64decode(r['kinesis']['data'])
            else:
                data = None   

            if data is not None:
                event_count += 1
                parse_cwevent_message( data, splhec )

            else:
                # If a given message doesn't follow our expected format it gets ignored so if we see a lot of these errors, it needs investigation
                print('Provided record did not have the key "kinesis" which is unexpected. Record was not parsed. Original record: %s ' % str(r))

        # Make sure that nothing is left in the object
        splhec.stop_threads_and_processing()

        '''
        Main execution complete. Check for failures, print measurement statements.
        '''
        print('CWLEvent lambda measurement:: context=%s num_total_records=%s total_events_seen=%s' % ( str(context.aws_request_id), str(len(event['Records'])), str(event_count) ))
        if splhec.backup_queue.empty():
            #print('No errors detected, moving on.')
            return {
                'statusCode': 200,
                'body': json.dumps('Processing all messages succeeded.')
            }
        else:
            # Let's import these only when they are needed.
            import time
            import hashlib
            import boto3
            print('ERROR: CWEvent processing resulting in %s delivery errors.' % (str(splhec.backup_queue.qsize())) )
            
            ### At this point, we have failures, we're going to take those and put them into files and upload to S3
            ### so that if the HEC were to completely die, we can still store files

            print('Creating connection to S3 bucket %s' % bucket_name)
            s3obj = boto3.client('s3')

            clear_failover_queue = True
            events_to_s3 = []

            while clear_failover_queue:
                if splhec.backup_queue.empty():
                    clear_failover_queue = False
                    break

                try:
                    #print('Attempting to get data from the failover queue.')
                    payload = splhec.backup_queue.get(False)
                except:
                    #print('No data exists.')
                    pass

                else:
                    #print('Data found. Adding to list that will be sent to S3 later.')
                    events_to_s3.append(payload)
                    splhec.backup_queue.task_done()


            try:                
                data_body = '\n'.join(events_to_s3)
                if s3_prefix.endswith('/'):
                    data_key = s3_prefix + str(time.strftime('%Y/%m/%d/%H')) + '/' + str(hashlib.md5(str(data_body).encode("utf-8")).hexdigest()) + '.log'
                else:
                    data_key = s3_prefix + '/' + str(time.strftime('%Y/%m/%d/%H')) + '/' + str(hashlib.md5(str(data_body).encode("utf-8")).hexdigest()) + '.log'

                #print('Attempting to write data to S3.')
                s3obj.put_object(Bucket=bucket_name, Key=data_key, Body=data_body)

                print('Failover succeeded. File write to s3://%s/%s' % ( str(bucket_name), str(data_key) ) )

                return {
                    'statusCode': 200,
                    'body': json.dumps('Data processed and stored in S3.')
                }   

            except Exception as ex:
                raise RuntimeError('Processing failed. Unable to failover to S3. Exception: %s.' % str(ex))
                return {
                    'statusCode': 500,
                    'body': json.dumps('Processing failed. Raising an exception so the lambda is seen as failed.')
                }   


    else:
        # This event is invalid from our expectations so it gets ignored.
        print('ERROR: Invalid event passed into the lambda. Original event: %s.' % str(json.dumps(event)))
        return {
            'statusCode': 200,
            'body': json.dumps('Invalid message was sent but setting status 200 because this should not require parsing again.')
        }


    return {
        'statusCode': 200,
        'body': json.dumps('Default return message. No other return happened.')
    }
