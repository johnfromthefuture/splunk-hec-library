# Overview
These functions are provided as-is and are meant to show examples of how I leverage the HEC library in this repository. Here are a few notes:

- In most cases, I need to pre-parse logs and route to different indexes based on the cloud account ID. So this is a thing in most of the functions.
- I make no claims to explain the differences between these environments but can confirm that all functions work when setup properly. 
- I leverage the 'source' field heavily to differentiate logs in my environment typically using a common 'sourcetype' for all logs from the same pipeline.
- Some modifications may be added to these functions for them to work properly in my primary production environment that are not here. Just how it is... 

So the summary? If you don't have an appetite for handling python and serverless debugging - or a heavy supply of whiskey in your cabinet - you probably want to avoid using some of these.

# Python Environments
Everything here uses python 3.6/3.7 environments depending on the provider. Nothing has been tested with previous versions of python.

# Future Development
It's highly unlikely that I'll maintain these functions outside of my production environment where the code is stored separately. If a major issue comes up, I'll backport the fix here but it's best to think of these as samples that you can adapt to your own needs. Have I said that enough yet?

# AWS Specifics
You'll need to setup IAM roles. Don't forget that part.

It's worth noting that a lot of effort was put into data segmentation and identifiers which drove many of the requirements behind this code. Therefore, it's overly complex and may be more than you need in a simpler environment. Make sure you review the code carefully and make appropriate modifications before adopting this in a production environment.


# Azure Specifics
Note that the Azure Function App listed here is a collection of multiple functions that I used to replace the functionality provided by the Azure Monitor app and this function app: https://github.com/Microsoft/AzureFunctionforSplunkVS. Nothing against those projects, I just had a desire for consistency across all my cloud environments and to leverage this library to make it happen. That said, you can follow the setup of those projects and use this function if you want with one notable exception: the setup of NSG Flow monitoring.

The basic setup for that is this:
- Configure NSG Flows to deliver to a storage account
- Register the Event Grid provider in your account
- Use an Event Subscription to route notifications of new blobs to an event hub in your event hub namespace
- Use this function to monitor that.

This particular function also uses (at the time of this writing) the preview version of the Azure SDK to access and download the storage blob. Bubblegum and tape, people, bubblegum and tape.

Also, good luck.

# GCP Specifics
Like Azure, good luck!
