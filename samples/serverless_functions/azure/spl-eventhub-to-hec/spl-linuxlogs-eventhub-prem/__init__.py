'''
Copyright (C) 2020 John Landers

This program is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
'''

import logging
import azure.functions as func

from datetime import datetime

import json
import time

import os

try:
    import Queue
except:
    import queue as Queue

import socket

from __app__.shared.splunk_hec import splunk_hec

'''

This function monitors hub insights-logs-diagnostics - Azure Diagnostic activity (metrics of things)

'''

# Setting up a concept for possible index differentiation down the road.
INDEX_DEFINITIONS = {
    'sample-azure-subscription-id': 
    {
        'default': 'main'
    },
    'default': 
    {
        'linux_os': 'linux',
        'linux_auditd': 'linux_auditd',
        'default': 'azure_diagnostics_logs'
    }
}

# I'm changing course on this from the Azure monitor app which, maybe I should have done on the other functions too
# but I don't believe having numerous sourcetypes is the correct approach to this log source.
DEFAULT_SOURCETYPE = 'azure:diagnostics:logs'

'''
I use this for handling OS environment variables.
'''
def normalize_str_to_bool(val):
    val = str(val).lower()
    if val == '1' or val == 'true' or val == 'yes':
        return True
    else:
        return False

def get_index_definitions(azure_account_id):
    try:
        if azure_account_id in INDEX_DEFINITIONS:
            defs=INDEX_DEFINITIONS[azure_account_id]
            if 'default' not in defs:
                defs['default'] = INDEX_DEFINITIONS['default']['default']

            return defs
        else:
            return INDEX_DEFINITIONS['default']
    except:
        return INDEX_DEFINITIONS['default']

def get_source_for_log(category, operation, azure_account_id, properties=None):
    if azure_account_id is None:
        azure_account_id = 'default'
    operation = str(operation).lower()
    if 'linux' in operation:      
        if properties is not None:
            if 'Msg' in properties:
                if 'audit(' in properties['Msg']:
                    return 'linux_auditd','azure:diagnosticlogs:%s:%s' % (str(azure_account_id), '/var/log/audit/audit.log')

        if 'audit' in category:
            return 'linux_auditd','azure:diagnosticlogs:%s:%s' % (str(azure_account_id), '/var/log/audit/audit.log')

        if 'auth' in category:
            return 'linux_os','azure:diagnosticlogs:%s:%s' % (str(azure_account_id), '/var/log/secure')

        return 'linux_os','azure:diagnosticlogs:%s:%s' % (str(azure_account_id), '/var/log/messages')

    
    # Default return
    return 'default','azure:diagnosticlogs:%s:%s' % (str(azure_account_id), str(operation).lower())


'''
'''
def main(event: func.EventHubEvent, storagefailover: func.Out[bytes]):

    # Add a backup queue
    splhec.backup_queue = Queue.Queue(0)

    # Set a context for log messages
    splhec.context_id = str('spl-diagnostics-eventhub-prem')

    try:
        records = json.loads(event.get_body().decode('utf-8'))
    except:
        raise RuntimeError('Unable to get and decode event record.')

    
    for r in records:
        payload = {}
        # Make a best effort to set the azure_account_id field
        resource_elements = r['resourceId'].split('/')
        try:
            # Assumption: /subscriptions/<subid>/... other stuff
            subscription = str(resource_elements[2]).lower()
            r['am_subscriptionId'] = subscription
            indexes = get_index_definitions(subscription)
            payload['fields'] = { 'azure_account_id': subscription } 
        except:
            indexes = INDEX_DEFINITIONS['default']
            subscription = None

        # These am_* fields were in the Azure Monitor app so I'm trying to maintain some compatibility
        # with work we've already done relative to this fields.
        try:
            r['am_resourceGroup'] = str(resource_elements[4]).lower()
            
        except:
            pass
        try:
            r['am_resourceName'] = str(resource_elements[8]).lower()
        except:
            pass
        try:
            r['am_resourceType'] = '%s/%s' % (str(resource_elements[6]).lower(), str(resource_elements[7]).lower())
        except:
            pass                    

        # Turns out, metrics can exist in this pipeline... sigh.
        if 'metricName' in r:
            payload['sourcetype'] = 'azure:diagnostics'
            payload['index'] = indexes['metrics']
            payload['source'] = 'azure:hec:%s:%s' % (str(resource_elements[6].split('.')[-1]), str(r['metricName']).lower())
            # Set a timestamp based on the recorded event.
            try:
                # This should work most of the time
                payload['time'] = int(datetime.strptime(r['time'][:-9], '%Y-%m-%dT%H:%M:%S').strftime('%s'))
            except:
                try:
                    # But some events have a different format
                    payload['time'] = int(datetime.strptime(r['time'][:-5], '%Y-%m-%dT%H:%M:%S').strftime('%s'))
                except:
                    # And finally, just failover
                    payload['time'] = int(time.time()) 

            if 'am_resourceName' in r:
                payload['host'] = str(r['am_resourceName']).lower()
            else:
                if subscription is not None:
                    payload['host'] = subscription
                else:
                    payload['host'] = 'unknown'

            payload['event'] = r
        # NOT METRICS, do this other thing instead!
        else:
            if 'am_resourceName' in r:
                payload['host'] = str(r['am_resourceName']).lower()
            else:
                if subscription is not None:
                    payload['host'] = subscription
                else:
                    payload['host'] = 'unknown'

            # Set a timestamp based on the recorded event.
            try:
                if 'properties' in r:
                    if 'EventTime' in r['properties']:
                        payload['time'] = int(datetime.strptime(r['properties']['EventTime'], '%Y-%m-%dT%H:%M:%S%z').strftime('%s'))
                    else:
                        payload['time'] = int(datetime.strptime(r['time'][:-9], '%Y-%m-%dT%H:%M:%S').strftime('%s'))
                else:
                    payload['time'] = int(datetime.strptime(r['time'][:-9], '%Y-%m-%dT%H:%M:%S').strftime('%s'))
            except:
                payload['time'] = int(time.time())

            # Set a source which needs to be based on a couple of items
            if 'properties' in r:
                index_type,payload['source'] = get_source_for_log(r['category'], r['operationName'], subscription, r['properties'])
            else:
                index_type,payload['source'] = get_source_for_log(r['category'], r['operationName'], subscription)
            
            # Set an index based on source
            try:
                if index_type in indexes:
                    payload['index'] = indexes[index_type]
                else:
                    payload['index'] = indexes['default']
            except:
                payload['index'] = indexes['default']

            # Set the default sourcetype
            payload['sourcetype'] = DEFAULT_SOURCETYPE

            # Construct a proper event from the json mess that was sent
            if 'LinuxSyslogEvent' in r['operationName']:
                msg = ''
                if 'properties' in r:
                    if 'EventTime' in r['properties']:
                        msg += r['properties']['EventTime']

                    if 'hostname' in r['properties']:
                        msg += ' %s' % (r['properties']['hostname'])

                    if 'ident' in r['properties']:
                        msg += ' [%s]' % (r['properties']['ident'])

                    if 'Msg' in r['properties']:
                        msg += ' %s' % (r['properties']['Msg'])
                
                if len(msg)>1:
                    payload['event'] = msg
                    payload['fields'].update({ 'azure_resource_group': r['am_resourceGroup'] })
                    payload['fields'].update({ 'azure_log_category': r['category'] })
                else:
                    if 'properties' in r:
                        payload['event'] = r['properties']
                    else:
                        payload['event'] = r
            else:
                payload['event'] = r
        

        #logging.info('payload test: %s' % str(json.dumps(payload)))
        splhec.send_event(json.dumps(payload, sort_keys=True))
    
    # All done flush the queue!
    splhec.force_flush_events()

    # Check the backup q
    if splhec.backup_queue.empty():
        logger.info('Backup queue was empty. Assume success.')
    else:
        logger.error('Backup queue has events. Clearing to blob storage.')
        #raise RuntimeError('Backup queue was not empty. Raise exception.')
        ## storagefailover.set(<text>)
        clear_failover_queue = True
        events_to_store = []
        while clear_failover_queue:
            if splhec.backup_queue.empty():
                clear_failover_queue = False
                break

            try:
                #print('Attempting to get data from the failover queue.')
                payload = splhec.backup_queue.get(False)
            except:
                #print('No data exists.')
                pass

            else:
                #print('Data found. Adding to list that will be sent to S3 later.')
                events_to_store.append(payload)
                splhec.backup_queue.task_done() 

        try:
            logger.info('Writing events to storage')
            events_to_store = '\n'.join(events_to_store)
            storagefailover.set(events_to_store)
            logger.info('Assuming successful write out if no exception.')
        except Exception as ex:
            raise RuntimeError('Processing failed. Unable to failover to blob storage. Exception: %s.' % str(ex))





### Globalizing these settings ###
try:
    hec_token = os.environ['HEC_TOKEN']
except:
    raise RuntimeError('HEC_TOKEN must be set in application settings.')

try:
    hec_server = os.environ['HEC_SERVER']
except:
    raise RuntimeError('HEC_SERVER must be set in application settings.')

try:
    hec_port = str(os.environ['HEC_PORT'])
except:
    hec_port='8088'

try:
    hec_ack = normalize_str_to_bool(os.environ['HEC_ACK'])
except:
    hec_ack = False

try:
    hec_ack_attempts = int(os.environ['HEC_ACK_ATTEMPTS'])
except:
    hec_ack_attempts = 10

try:
    hec_max_content_length = int(os.environ['HEC_MAX_CONTENT_LENGTH'])
except:
    hec_max_content_length = 75000000

try:
    hec_batch_size = int(os.environ['HEC_BATCH_SIZE'])
except:
    hec_batch_size = 75000

try:
    enable_debug = normalize_str_to_bool(os.environ['ENABLE_DEBUG'])
except:
    enable_debug = False

# Create a logger
logger = logging.getLogger(__name__)

if enable_debug:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

splhec = splunk_hec(token=hec_token, hec_server=hec_server, hec_port=hec_port, input_type='json', use_hec_tls=True,
    use_ack=hec_ack, hec_tls_verify=False, max_ack_attempts=hec_ack_attempts, max_content_length=hec_max_content_length,
    max_events_per_batch=hec_batch_size, debug_enabled=enable_debug, logger=logger )

