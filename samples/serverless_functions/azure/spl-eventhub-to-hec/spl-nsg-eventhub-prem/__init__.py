'''
Copyright (C) 2020 John Landers

This program is free software: you can redistribute it and/or modify it under the terms of the 
GNU General Public License as published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even 
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 
'''


import logging
import azure.functions as func

from datetime import datetime

import json
import time
import io

import os

try:
    import Queue
except:
    import queue as Queue


from azure.storage.blob import BlobClient
from __app__.shared.splunk_hec import splunk_hec

'''
This function monitors the NSG hub which contains event grid subscrpition events telling us where to find storage blobs to download.

    Requires NSGSTORAGEACCESSKEY environment variable to access the right storage.

'''
# Setting up a concept for possible index differentiation down the road.

INDEX_DEFINITIONS = {
    'sample-azure-subscription-id': 'main',
    'default': 'azure_nsg_flows'
}


'''
I use this for handling OS environment variables.
'''
def normalize_str_to_bool(val):
    val = str(val).lower()
    if val == '1' or val == 'true' or val == 'yes':
        return True
    else:
        return False


def main(event: func.EventHubEvent, storagefailover: func.Out[bytes]):

    # Add a backup queue
    splhec.backup_queue = Queue.Queue(0)

    # Set a context for log messages
    splhec.context_id = str('spl-nsg-eventhub-prem')

    try:
        ehrecords = json.loads(event.get_body().decode('utf-8'))
    except:
        raise RuntimeError('Unable to get and decode event record.')

    # It seems like sometimes we might get multiple records in a list and other times we do not? This compensates for that.
    if type(ehrecords) is not list:
        ehrecords = [ehrecords]

    #logging.info('Debug message. Payload is: %s' % (json.dumps(ehrecords)))

    # When the data comes into the function, it's a list inside a list... Not entirely sure why. So this helps with that...
    for r in ehrecords:
        for record in r:
            # I expect this exists in the incoming record
            blob_url = record['data']['url']

            # Using this plus the shared access key to create a client
            blob_client = BlobClient(blob_url=blob_url, credential=os.environ['NSGSTORAGEACCESSKEY'])

            # we'll download the blob into the bytes_buffer - memory - avoiding touching local disk
            bytes_buffer = io.BytesIO()
            bytes_buffer.writelines(blob_client.download_blob())

            try:
                # Here we decode it
                data = json.loads((bytes_buffer.getvalue()).decode('utf-8', 'ignore'))
            except Exception as ex1:
                raise RuntimeError('Unable to download blob and decode the storage blob. Blob: %s, Exception: %s' % (str(blob_url), str(ex1)))

            if 'records' in data:
                logging.info('Records exist, looping.')
                # loop through all the available records
                for record in data['records']:
                    '''
                        Need to check: record['properties']['Version']
                            Version 2 has more fields.

                        record['properties']['flows']

                        >>> type(record['properties']['flows'])
                        <class 'list'>


                    '''
                    # Extract the subscription from this value #
                    resource = record['resourceId'].split('/')
                    subscription_id = resource[2].lower()
                    nsg = resource[-1].lower()
                    

                    # Treating this like it's an instance ID in AWS
                    system_id = record['systemId']

                    for rules in record['properties']['flows']:
                        '''
                            >>> print(json.dumps(record['properties']['flows'][0], sort_keys=True, indent=4))
                            {
                                "flows": [
                                    {
                                        "flowTuples": [
                                            "1566576000,182.254.229.58,10.142.10.36,45609,445,T,I,D,B,,,,",
                                            "1566576031,185.176.27.46,10.142.10.36,57984,33235,T,I,D,B,,,,"
                                        ],
                                        "mac": "000D3A06D9B5"
                                    }
                                ],
                                "rule": "DefaultRule_DenyAllInBound"
                            }

                        '''
                        rule_name = rules['rule']

                        for flow in rules['flows']:
                            '''
                                >>> print(json.dumps(rules['flows'][0],sort_keys=True,indent=4))
                                {
                                    "flowTuples": [
                                        "1566576000,182.254.229.58,10.142.10.36,45609,445,T,I,D,B,,,,",
                                        "1566576031,185.176.27.46,10.142.10.36,57984,33235,T,I,D,B,,,,"
                                    ],
                                    "mac": "000D3A06D9B5"
                                }

                            '''
                            mac_address = flow['mac']

                            # Reference: https://docs.microsoft.com/en-us/azure/network-watcher/network-watcher-nsg-flow-logging-overview
                            for flowtuple in flow['flowTuples']:
                                payload = {}
                                if subscription_id in INDEX_DEFINITIONS:
                                    payload['index'] = INDEX_DEFINITIONS[subscription_id]
                                else:
                                    payload['index'] = INDEX_DEFINITIONS['default']

                            
                                payload['source'] = 'azure:nsgflows:%s:%s' % (subscription_id, nsg)
                                payload['sourcetype'] = 'azure:function:nsgflows'
                                payload['host'] = system_id

                                payload['fields'] = {}
                                payload['fields'].update({'azure_account_id': subscription_id})

                                payload['event'] = {}
                                payload['event']['rule_name'] = rule_name
                                payload['event']['mac_address'] = mac_address

                                '''
                                "1566576000,182.254.229.58,10.142.10.36,45609,445,T,I,D,B,,,,"
                                
                                Expecting 13 fields in version 2...
                                '''
                                csvsplit = flowtuple.split(',')

                                # These fields, I expect to exist always:
                                
                                payload['time'] = csvsplit[0]                     
                                payload['event']['src_ip'] = csvsplit[1]
                                payload['event']['dest_ip'] = csvsplit[2]
                                payload['event']['src_port'] = csvsplit[3]
                                payload['event']['dest_port'] = csvsplit[4]

                                try:
                                    if csvsplit[5] == 'T':
                                        payload['event']['transport'] = 'TCP'
                                    elif csvsplit[5] == 'U':
                                        payload['event']['transport'] = 'UDP'
                                except:
                                    payload['event']['transport'] = csvsplit[5]

                                try:
                                    if csvsplit[6] == 'I':
                                        payload['event']['direction'] = 'inbound'
                                    elif csvsplit[6] == 'O':
                                        payload['event']['direction'] = 'outbound'
                                    else:
                                        payload['event']['direction'] = csvsplit[6]

                                except:
                                    payload['event']['direction'] = csvsplit[6]

                                try:
                                    if csvsplit[7] == 'A':
                                        payload['event']['action'] = 'allowed'
                                    elif csvsplit[7] == 'D':
                                        payload['event']['action'] = 'denied'
                                    else:
                                        payload['event']['action'] = csvsplit[7]

                                except:
                                    payload['event']['action'] = csvsplit[7]

                                ################## These fields are for version 2 only ######################
                                try:
                                    if csvsplit[8] == 'B':
                                        payload['event']['flow_state'] = 'begin'
                                    elif csvsplit[8] == 'C':
                                        payload['event']['flow_state'] = 'continuing'
                                    elif csvsplit[8] == 'E':
                                        payload['event']['flow_state'] = 'end'
                                    else:
                                        payload['event']['flow_state'] = csvsplit[8]

                                    payload['event']['packets_out'] = csvsplit[9]
                                    payload['event']['bytes_out'] = csvsplit[10]
                                    payload['event']['packets_in'] = csvsplit[11]
                                    payload['event']['bytes_in'] = csvsplit[12]                            

                                except KeyError:
                                    # ignoring key errors because version 1 doesn't have these fields
                                    pass

                                splhec.send_event(json.dumps(payload))


    ######################                   
    
    # All done flush the queue!
    splhec.force_flush_events()

    # Check the backup q
    if splhec.backup_queue.empty():
        logger.info('Backup queue was empty. Assume success.')
    else:
        logger.error('Backup queue has events. Clearing to blob storage.')
        #raise RuntimeError('Backup queue was not empty. Raise exception.')
        ## storagefailover.set(<text>)
        clear_failover_queue = True
        events_to_store = []
        while clear_failover_queue:
            if splhec.backup_queue.empty():
                clear_failover_queue = False
                break

            try:
                #print('Attempting to get data from the failover queue.')
                payload = splhec.backup_queue.get(False)
            except:
                #print('No data exists.')
                pass

            else:
                #print('Data found. Adding to list that will be sent to S3 later.')
                events_to_store.append(payload)
                splhec.backup_queue.task_done() 

        try:
            logger.info('Writing events to storage')
            events_to_store = '\n'.join(events_to_store)
            storagefailover.set(events_to_store)
            logger.info('Assuming successful write out if no exception.')
        except Exception as ex:
            raise RuntimeError('Processing failed. Unable to failover to blob storage. Exception: %s.' % str(ex))





### Globalizing these settings ###
try:
    hec_token = os.environ['HEC_TOKEN']
except:
    raise RuntimeError('HEC_TOKEN must be set in application settings.')

try:
    hec_server = os.environ['HEC_SERVER']
except:
    raise RuntimeError('HEC_SERVER must be set in application settings.')

try:
    hec_port = str(os.environ['HEC_PORT'])
except:
    hec_port='8088'

try:
    hec_ack = normalize_str_to_bool(os.environ['HEC_ACK'])
except:
    hec_ack = False

try:
    hec_ack_attempts = int(os.environ['HEC_ACK_ATTEMPTS'])
except:
    hec_ack_attempts = 10

try:
    hec_max_content_length = int(os.environ['HEC_MAX_CONTENT_LENGTH'])
except:
    hec_max_content_length = 75000000

try:
    hec_batch_size = int(os.environ['HEC_BATCH_SIZE'])
except:
    hec_batch_size = 75000

try:
    enable_debug = normalize_str_to_bool(os.environ['ENABLE_DEBUG'])
except:
    enable_debug = False

# Create a logger
logger = logging.getLogger(__name__)

if enable_debug:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

splhec = splunk_hec(token=hec_token, hec_server=hec_server, hec_port=hec_port, input_type='json', use_hec_tls=True,
    use_ack=hec_ack, hec_tls_verify=False, max_ack_attempts=hec_ack_attempts, max_content_length=hec_max_content_length,
    max_events_per_batch=hec_batch_size, debug_enabled=enable_debug, logger=logger )

